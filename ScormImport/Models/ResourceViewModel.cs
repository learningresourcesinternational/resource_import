﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScormImport.Models
{
	public class ResourceViewModel
	{
		// Primary Key...
		public int LearningResourceID { get; set; }

		// Base info...
		public ResourceBaseInfo BaseInfo { get; set; }		

		// Child info...
		public List<ResourceChildInfo> ChildrenResources { get; set; }

	}
}
