﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScormImport.Models
{
	public class ResourceBaseInfo
	{
		public ResourceBaseInfo()
		{
			Publisher = "Jenison";
			PublishDate = DateTime.Today;
			Author = "LRI";
			Length = "60 Mins";
			Cost = 0;
			CanBeBorrowed = true; // Absolutlety stupid! All resource must be marked as true here, otherwise academy thinks it is a flexicentre resource!
			Tracked = true;
			CPDEligible = false;
			CPDEligiblePoints = 0;
			ISBN = "NOMENU";
			IsActive = true;
			OverrideApproval = 0;
			RequiresFeedback = true;
			UpdatedBy = 1;
		}

		public string Title { get; set; }
		public string ManifestResourceTitle { get; set; }
		public string Reference { get; set; }
		public string Description { get; set; }
		public string Publisher { get; set; }
		public DateTime PublishDate { get; set; }
		public string Author { get; set; }
		public string Length { get; set; }
		public decimal Cost { get; set; }
		public bool CanBeBorrowed { get; set; }
		public bool Tracked { get; set; }
		public bool Scored { get; set; }
		public bool CPDEligible { get; set; }
		public decimal CPDEligiblePoints { get; set; }
		public string ISBN { get; set; }
		public bool IsActive { get; set; }
		public int OverrideApproval { get; set; } // ONLY DP WOLRD HAS INT FOR APPROVAL FLAG
		public bool RequiresFeedback { get; set; }		
		public string ScormCoursePath { get; set; }
		public string ScormIMSPath { get; set; }
		public string AICCLaunchURL { get; set; }
		public string DefaultCategoryStructure { get; set; }
		public int UpdatedBy { get; set; }
		

	}

	public class ResourceMetaData
	{
		public ResourceMetaData()
		{
			Synopsis = "- no synopsis provided -";
			Description = "- no synopsis provided -";
			EvaluationID = 12;
		}

		public string Synopsis { get; set; }
		public string Description { get; set; }
		public int EvaluationID { get; set; }		
	}
}
