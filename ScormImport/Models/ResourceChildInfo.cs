﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScormImport.Models
{
	public class ResourceChildInfo
	{
		public int LearningResourceID { get; set; }
		public string LaunchURL { get; set; }
		public int ParentID { get; set; }
	}
}
