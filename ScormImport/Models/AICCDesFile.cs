﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileHelpers;

namespace ScormImport.Models
{
	[DelimitedRecord(",")]
	[IgnoreFirst]
	public class AICCDesFile
	{
		public string System_ID;
		public string Developer_ID;
		[FieldQuoted('"', QuoteMode.OptionalForBoth, MultilineMode.NotAllow)]
		public string Title;
		[FieldQuoted('"', QuoteMode.OptionalForBoth, MultilineMode.AllowForBoth)]
		public string Description;		
	}
}
