﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace ScormImport.Domain
{
	public class SCOFacade
	{
		// SCO Data = Item Data + Resource Data...
		public List<ScormItem> Items { get; set; }

		private readonly XMLFacade xmlLoader;
		private readonly ScormItem item;

		public SCOFacade(string xmlFile)
		{
			XMLFacade xmlLoader = new XMLFacade();
			ScormItem item = new ScormItem();

			// Load xml DOC...
			XmlDocument xmlDoc = xmlLoader.LoadDocument(xmlFile);

			// Get Items...
			this.Items = item.GetItems(xmlDoc);

		}

	}

	public class ScormItem
	{
		public string Title { get; set; }
		public string Identifier { get; set; }
		public string IdentifierRef { get; set; }
		public string MasteryScore { get; set; }
		public List<ScormResource> Resources { get; set; }

		public readonly ScormResource res;

		public ScormItem()
		{
			res = new ScormResource();
			Resources = new List<ScormResource>();
		}

		public List<ScormItem> GetItems(XmlDocument xmlDoc)
		{
			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:manifest", nsManager);
			XmlNodeList xmlNodes = root.SelectNodes("xns:organizations/xns:organization/xns:item", nsManager);

			List<ScormItem> itemList = new List<ScormItem>();

			if (xmlNodes != null)
			{
				foreach (XmlNode node in xmlNodes)
				{
					// Create new instance...
					ScormItem myItem = new ScormItem();

					// Make sure we at least have a title...
					if (node["title"] != null)
					{
						myItem.Title = node["title"].InnerText;
						myItem.MasteryScore = node["adlcp:masteryscore"] != null ? node["adlcp:masteryscore"].InnerText : "0";
						myItem.Identifier = node.Attributes.GetNamedItem("identifier").InnerText;
						myItem.IdentifierRef = node.Attributes.GetNamedItem("identifierref").InnerText;
						myItem.Resources = res.GetResources(xmlDoc, myItem.IdentifierRef);

						itemList.Add(myItem);
					}
					
				}

			}

			return itemList;
		}

	}

	public class ScormResource
	{
		private readonly XMLFacade xmlLoader;

		public ScormResource()
		{
			XMLFacade xmlLoader = new XMLFacade();

			// Initalise List of files, we could do this in the declaration but decided to do it here...
			this.Files = new List<string>();
		}

		public string adlcp { get; set; }
		public string Identifier { get; set; }
		public string Type { get; set; }
		public string ScormType { get; set; }
		public string Href { get; set; }
		public List<string> Files { get; set; }

		public List<ScormResource> GetResources(string xmlFile)
		{
			XmlDocument xmlDoc = xmlLoader.LoadDocument(xmlFile);

			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:manifest", nsManager);

			XmlNodeList xmlNodes = root.SelectNodes("xns:resources/xns:resource", nsManager);

			// Gonna do this slightly different to normal...
			List<ScormResource> resources = new List<ScormResource>();

			if (xmlNodes != null)
			{
				foreach (XmlNode node in xmlNodes)
				{
					ScormResource res = new ScormResource();

					res.adlcp = root.Attributes.GetNamedItem("xmlns:adlcp").InnerText;

					res.Identifier = node.Attributes.GetNamedItem("identifier").InnerText;
					res.Type = node.Attributes.GetNamedItem("type").InnerText;
					res.ScormType = node.Attributes.GetNamedItem("adlcp:scormtype").InnerText;

					// Apparently href should be mandatory but int the example manifest, it isn't, so...
					if (node.Attributes.GetNamedItem("href") != null)
					{
						res.Href = node.Attributes.GetNamedItem("href").InnerText;
					}

					// Do we have any files (we probably should)...
					if (node.HasChildNodes)
					{
						XmlNodeList xmlFiles = node.ChildNodes;
						res.Files = res.GetFiles(nsManager, root, xmlFiles, null);
					}

					resources.Add(res);
				}
			}

			return resources;
		}

		public List<ScormResource> GetResources(XmlDocument xmlDoc, string IdentifierRef)
		{
			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:manifest", nsManager);

			XmlNodeList xmlNodes = root.SelectNodes("xns:resources/xns:resource[@identifier='" + IdentifierRef + "']", nsManager);

			// Gonna do this slightly different to normal...
			List<ScormResource> resources = new List<ScormResource>();

			if (xmlNodes != null)
			{
				foreach (XmlNode node in xmlNodes)
				{
					ScormResource res = new ScormResource();

					res.adlcp = root.Attributes.GetNamedItem("xmlns:adlcp").InnerText;

					res.Identifier = node.Attributes.GetNamedItem("identifier").InnerText;
					res.Type = node.Attributes.GetNamedItem("type").InnerText;
					res.ScormType = node.Attributes.GetNamedItem("adlcp:scormtype").InnerText;

					// Apparently href should be mandatory but int the example manifest, it isn't, so...
					if (node.Attributes.GetNamedItem("href") != null)
					{
						res.Href = node.Attributes.GetNamedItem("href").InnerText;
					}

					// Do we have any files (we probably should)...
					if (node.HasChildNodes)
					{
						XmlNodeList xmlFiles = node.ChildNodes;
						res.Files = res.GetFiles(nsManager, root, xmlFiles, null);
					}

					resources.Add(res);
				}
			}

			return resources;
		}

		private List<string> GetFiles(XmlNamespaceManager nsManager, XmlNode root, XmlNodeList xmlFiles, List<string> resFiles)
		{
			if (resFiles == null)
			{
				resFiles = new List<string>();
			}

			foreach (XmlNode file in xmlFiles)
			{
				if (file.Name == "file")
				{
					resFiles.Add(file.Attributes.GetNamedItem("href").InnerText);
				}
				else if (file.Name == "dependency")
				{
					// Get files based on dependency resource identifier...
					XmlNode xmlRes = root.SelectSingleNode("xns:resources/xns:resource[@identifier='" + file.Attributes.GetNamedItem("identifierref").InnerText + "']", nsManager);
					if (xmlRes.HasChildNodes)
					{
						XmlNodeList xmlDependFiles = xmlRes.ChildNodes;
						GetFiles(nsManager, root, xmlDependFiles, resFiles);
					}
				}
			}

			return resFiles;

		}
	}

	public class Organisation
	{
		public string Title { get; set; }
		public string ItemTitle { get; set; }
		public string Identifier { get; set; }
		public string IdentifierRef { get; set; }
		public string MasteryScore { get; set; }
		public string LaunchData { get; set; }

		private readonly XMLFacade xmlLoader;

		public Organisation() 
		{
			XMLFacade xmlLoader = new XMLFacade();
		}

		public List<Organisation> GetOrganisation(string xmlFile)
		{
			XmlDocument xmlDoc = xmlLoader.LoadDocument(xmlFile);

			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:manifest", nsManager);

			XmlNodeList xmlNodes = root.SelectNodes("xns:organizations/xns:organization", nsManager);

			// Gonna do this slightly different to normal...
			List<Organisation> orgs = new List<Organisation>();
			Organisation org = new Organisation();

			if (xmlNodes != null)
			{
				foreach (XmlNode node in xmlNodes)
				{

					org.Identifier = node.Attributes.GetNamedItem("identifier").InnerText;
					org.Title = node["title"].InnerText;

					// Do we have any items (we probably should)...
					if (node.HasChildNodes)
					{
						XmlNodeList xmlItems = node.SelectNodes("xns:item", nsManager);

						foreach (XmlNode item in xmlItems)
						{
							org.IdentifierRef = item.Attributes.GetNamedItem("identifierref").InnerText;
							org.ItemTitle = item["title"].InnerText;
							org.MasteryScore = item["adlcp:masteryscore"].InnerText;
						}
					}

					orgs.Add(org);
				}
			}

			return orgs;
		}
	}



}