﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScormImport.Domain
{
	public class XMLFacade
	{
		public XmlDocument LoadDocument(string xmlFilename)
		{
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load(System.IO.Path.GetFullPath(xmlFilename));

			return xmlDoc;

		}
	}
}
