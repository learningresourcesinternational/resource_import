﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScormImport.DAL;
using ScormImport.Models;

namespace ScormImport.Domain
{
	public class ImportFacade
	{
		private readonly string[] _manifestList;
		private SCOFacade _scorm;
		private IProductRepository _repository;
		private XMLMappingRepository _xml;
		private readonly string[] _BaseFolderList;
		private string version = " (new)";
		private string _baseFolder = string.Empty;
		private string _virtualDirectory = string.Empty;
		private string _category = string.Empty;
		private string _courseDescription = "- no synopsis provided -";

		public ImportFacade(string[] manifests, IProductRepository repo, string coursePath, string category, string description)
		{
			_manifestList = manifests;
			_repository = repo;
			_xml = new XMLMappingRepository();
			_BaseFolderList = new string[] { "DATA_PROTECTION", "ESSENTIALS", "ESSENTIALS_TESTS", "EXPRESS", "INSIGHTS", "INSIGHTS CLASSICS", "PATHWAYS", "SHAPERS", "WORKPLACE", "ManagementExcellence", "Office 2010" };
			_virtualDirectory = coursePath;
			_category = category;
			_courseDescription = description;
		}

		//TODO: Produce log of what courses have been imported and to which category
		public void WriteLogFileOfResources()
		{
			throw new NotImplementedException();
		}

		public void ImportScormResources()
		{
			ResourceViewModel parent;

			foreach (string manifest in _manifestList)
			{
				_scorm = new SCOFacade(manifest);
				SetBaseFolder(manifest);

				// Get the main item...
				ScormItem scormItem = _scorm.Items.FirstOrDefault();

				if (!_repository.DoesResourceExist(scormItem.Title + version))
				{
					// Create parent record...
					parent = CreateParent(scormItem, manifest);

					if (parent != null)
					{
						// Create child record(s)...
						foreach (ScormResource item in scormItem.Resources)
						{
							if (item != null)
							{
								// Create child an updated resource view model...
								parent = CreateChild(item, parent);
							}
						}

						// Update description, synopsis etc based on older versions...
						UpdateAncillaryInfo(parent);
					}
				}				
			}

		}

		public void ImportAICCResources()
		{
			ResourceViewModel parent;

			foreach (string manifest in _manifestList)
			{
				//TODO: Create a wrapper around the FileHelpers code...
				AICCDesFile[] results = null;
				SetBaseFolder(manifest);

				FileHelpers.FileHelperEngine engine = new FileHelpers.FileHelperEngine(typeof(AICCDesFile));
				results = engine.ReadFile(manifest) as AICCDesFile[];

				if (!_repository.DoesResourceExist(results[0].Title + version))
				{

					ResourceBaseInfo baseInfo = new ResourceBaseInfo();
					int refLen = 5;

					baseInfo.Title = results[0].Title.Trim() + version;
					baseInfo.ManifestResourceTitle = results[0].Title.Trim();
					baseInfo.Author = "LRI";
					baseInfo.Publisher = "LRI";
					baseInfo.Description = _courseDescription;

					if (results[0].Title.Trim().Length < 5)
					{
						refLen = 3;
					}

					baseInfo.Reference = results[0].Title.Substring(0, refLen).ToUpper() + "1.4";

					// AICC, so just have to assume...
					baseInfo.Scored = true;

					// AICC Path...
					baseInfo.AICCLaunchURL = FindRecommendedCoursePath(manifest) + "/index_lms.html";

					baseInfo.DefaultCategoryStructure = _category;
					baseInfo.Length = "60 - 90 mins";

					UpdateAncillaryInfo(_repository.SaveParent(baseInfo));
				}

			}
		}

		private ResourceViewModel CreateParent(ScormItem item, string imspath)
		{
			
			ResourceBaseInfo baseInfo = new ResourceBaseInfo();
			int refLen = 5;

			baseInfo.Title = item.Title.Trim() + version;
			baseInfo.ManifestResourceTitle = item.Title.Trim();

			if (item.Title.Length < 5)
			{
				refLen = 3;
			}

			baseInfo.Reference = item.Title.Substring(0, refLen).ToUpper() + "1.4";
			baseInfo.Description = _courseDescription;

			// If no mastery score assume no test...
			baseInfo.Scored = false;
			if (item.MasteryScore != "0")
			{
				baseInfo.Scored = true;
			}

			// Scorm Paths...
			baseInfo.ScormIMSPath = imspath;
			baseInfo.ScormCoursePath = FindRecommendedCoursePath(imspath);

			baseInfo.DefaultCategoryStructure = _category;
			baseInfo.Length = SetDefaultLength();

			return _repository.SaveParent(baseInfo);
		}

		private ResourceViewModel CreateChild(ScormResource item, ResourceViewModel resource)
		{
			ResourceChildInfo childItem = new ResourceChildInfo();

			childItem.LaunchURL = item.Href;
			childItem.ParentID = resource.LearningResourceID;

			// Instantiate child elements...
			resource.ChildrenResources = new List<ResourceChildInfo>();

			return _repository.SaveChildElements(childItem, resource);

		}

		private void UpdateAncillaryInfo(ResourceViewModel resource)
		{
			ResourceMetaData meta = new ResourceMetaData();

			//if (resource.BaseInfo.Description != null)
			//{
			//	meta.Description = resource.BaseInfo.Description;
			//	meta.Synopsis = resource.BaseInfo.Description;
			//}

			_repository.SaveMetaData(meta, resource);
		}

		private string FindRecommendedCoursePath(string manifest)
		{
			string[] manifestArray = manifest.Split('\\');

			int baseFolderIndex = 0;

			// Find which base folder we came from and then the index...			
			baseFolderIndex = Array.IndexOf(manifestArray, _baseFolder);

			//TODO: Incorporate the version number (in this case 1.4) into the course path, so that it is in the 
			//final launch path.  This means we can simply map to jenison rather than jenison 1.4

			// Check if we have a trailing '/', if not add one...
			string lastChar = _virtualDirectory.Substring(_virtualDirectory.Length - 1);
			if (lastChar != "/")
			{
				_virtualDirectory += "/";
			}

			string path = _virtualDirectory;

			// Build course path...
			for (int i = baseFolderIndex; i < manifestArray.Length - 1; i++)
			{
				path += manifestArray[i] + "/";
			}

			if (path != string.Empty)
			{
				return path;
			}
			else
			{
				throw new Exception("Cannot find SCORM Course Path");
			}
		}

		private string SetDefaultCategory(string manifest, string resourceName)
		{
			//string[] manifestArray = manifest.Split('\\');
			string baseFolder = string.Empty;

			//XMLMappingRepository xml = new XMLMappingRepository();

			//baseFolder = _BaseFolderList.Where(w => manifestArray.Contains(w)).FirstOrDefault();

			// Remove any underscores '_' from base folder...
			baseFolder = _baseFolder.Replace("_", " ");

			//TODO: This should be replaced with something more reliable / the xml doc courses problems with office courses as some courses have same name
			//such as printing... May just do a search for Excel, Word, Beginners etc... or amend program to accept the category and do what set at a time.
			if (baseFolder == "Office 2010")
			{
				string[] manifestArray = manifest.Split('\\');

				return manifestArray[4] + "\\" + manifestArray[5] + "\\" + manifestArray[6];
			}
			else
			{
				return _xml.GetRecommendedCategoryStructure(_baseFolder, resourceName);
			}

		}

		private void SetBaseFolder(string manifest)
		{
			string[] manifestArray = manifest.Split('\\');
			string baseFolder = string.Empty;

			XMLMappingRepository xml = new XMLMappingRepository();

			_baseFolder = _BaseFolderList.Where(w => manifestArray.Contains(w)).FirstOrDefault();

		}

		private string SetDefaultLength()
		{
			switch (_baseFolder)
			{				
				case "DATA_PROTECTION":
					return "20 - 30 mins";
				case "ESSENTIALS": 
					return "60 - 120 mins";
				case "ESSENTIALS_TESTS": 
					return "20 - 30 mins";
				case "EXPRESS": 
					return "5 - 15 mins";
				case "INSIGHTS": 
					return "15 - 30 mins";
				case "INSIGHTS CLASSICS": 
					return "15 - 30 mins";
				case "PATHWAYS": 
					return "15 - 40 mins";
				case "SHAPERS": 
					return "20 - 30 mins";
				case "WORKPLACE":
					return "90 - 120 mins";				
				default:
					return "20 - 60 mins";
			}
		}

		public void TestCategory()
		{
			foreach (string manifest in _manifestList)
			{
				_scorm = new SCOFacade(manifest);
				SetBaseFolder(manifest);

				// Get the main item...
				ScormItem scormItem = _scorm.Items.FirstOrDefault();

				string x = SetDefaultCategory(manifest, scormItem.Title);
				
			}
		}
	}
}
