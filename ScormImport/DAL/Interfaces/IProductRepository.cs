﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScormImport.Models;

namespace ScormImport.DAL
{
	public interface IProductRepository
	{
		ResourceViewModel SaveParent(ResourceBaseInfo model);
		ResourceViewModel SaveChildElements(ResourceChildInfo model, ResourceViewModel viewModel);
		ResourceViewModel SaveMetaData(ResourceMetaData model, ResourceViewModel viewModel);
		bool DoesResourceExist(string title);
	}
}
