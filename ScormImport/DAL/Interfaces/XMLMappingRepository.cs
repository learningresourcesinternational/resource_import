﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScormImport.DAL
{
	public class XMLMappingRepository : IMappingRepository
	{
		public string GetRecommendedCategoryStructure(string baseSector, string resourceName)
		{
			string catStructure = string.Empty;

			// Load xml DOC...
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("D:\\Development\\Workbench\\ScormImport\\ScormImport\\Mapping Document\\WorkingMappingDoc.xml");

			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:data-set", nsManager);
			XmlNodeList xmlNodes = null;

			// What is our base sector...
			switch (baseSector)
			{
				case "DATA_PROTECTION":
					return "Data Protection";
				case "ESSENTIALS_TESTS":
					catStructure = "ESSENTIALS\\";
					break;
				case "INSIGHTS":
					return "Insights";
				default:
					catStructure = baseSector + "\\";
					break;
			}

			xmlNodes = root.SelectNodes("//Resource", nsManager);

			if (xmlNodes != null)
			{
				foreach (XmlNode node in xmlNodes)
				{					
					if (node["Name"].InnerText.ToLower() == resourceName.ToLower())
					{
						string xtc = node["Name"].InnerText.ToLower();
					}
					if (node["BaseSector"].InnerText.ToLower() == baseSector.ToLower() && node["Name"].InnerText.ToLower() == resourceName.ToLower())
					{
						//if (node["Category"].InnerText.ToLower() == "Data Protection")
						//{
						//	catStructure = "Data Protection";
						//}
						//else
						//{
							catStructure += node["Category"].InnerText;
						//}
						break;
					}
				}
				
			}

			return catStructure;
		}

		public bool IsResourceMapped(string baseSector, string resourceName)
		{
						// Load xml DOC...
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.Load("D:\\Development\\Workbench\\ScormImport\\ScormImport\\Mapping Document\\WorkingMappingDoc.xml");

			// Create ns manager...
			XmlNamespaceManager nsManager = new XmlNamespaceManager(xmlDoc.NameTable);
			nsManager.AddNamespace("xns", xmlDoc.DocumentElement.NamespaceURI);

			XmlNode root = xmlDoc.SelectSingleNode("//xns:data-set", nsManager);
			XmlNodeList xmlNodes = root.SelectNodes("//Resource[contains(BaseSector, '" + baseSector + "') and contains(Name, '" + resourceName + "')]", nsManager);

			if (xmlNodes != null)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

	}
}
