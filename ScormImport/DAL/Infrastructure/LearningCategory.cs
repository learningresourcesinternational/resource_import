//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScormImport.DAL.Infrastructure
{
    using System;
    using System.Collections.Generic;
    
    public partial class LearningCategory
    {
        public LearningCategory()
        {
            this.LearningCategories1 = new HashSet<LearningCategory>();
            this.LearningResourcesCategories = new HashSet<LearningResourcesCategory>();
        }
    
        public int LearningCategoryID { get; set; }
        public string LearningCategoryName { get; set; }
        public string LearningCategoryRef { get; set; }
        public string LearningCategoryDescription { get; set; }
        public Nullable<int> LearningCategoryImageFile { get; set; }
        public System.DateTime LearningCategoryAddedOn { get; set; }
        public int LearningCategoryAddedByUserID { get; set; }
        public Nullable<System.DateTime> LearningCategoryUpdatedOn { get; set; }
        public bool LearningCategoryAvailable { get; set; }
        public Nullable<int> ParentCategoryID { get; set; }
        public int SystemObjectsStatusID { get; set; }
        public int UserLastUpdatingRecordID { get; set; }
    
        public virtual ICollection<LearningCategory> LearningCategories1 { get; set; }
        public virtual LearningCategory LearningCategory1 { get; set; }
        public virtual ICollection<LearningResourcesCategory> LearningResourcesCategories { get; set; }
    }
}
