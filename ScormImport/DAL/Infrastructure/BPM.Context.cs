﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ScormImport.DAL.Infrastructure
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BPMEntities : DbContext
    {
        public BPMEntities()
            : base("name=BPMEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<ResourcesUnit> ResourcesUnits { get; set; }
        public virtual DbSet<Unit> Units { get; set; }
        public virtual DbSet<ILMCours> ILMCourses { get; set; }
        public virtual DbSet<ILMCoursesUnit> ILMCoursesUnits { get; set; }
    }
}
