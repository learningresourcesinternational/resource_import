﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ScormImport.DAL.Infrastructure;
using ScormImport.Models;

namespace ScormImport.DAL.Repos
{
	public class BPMRepository : IProductRepository
	{
		BPMEntities context = new BPMEntities();		

		public ResourceViewModel SaveParent(Models.ResourceBaseInfo model)
		{
			// Map base info to learning resource...			
			InitialiseMapping();
			Resource res = Mapper.Map<ResourceBaseInfo, Resource>(model);

			// Academy specifics...
			res.MediaTypeID = 5;
			res.LearningResourceCostCurrencyTypeID = 30;
			res.LearningResourceIsCourseAuthored = false;
			res.LearningResourceTrack = true;			
			res.ParentLearningResourceID = 0;
			res = SetAuditData(res);

			// Save everything here...
			context.Resources.Add(res);
			context.SaveChanges();

			// Create View Model...
			ResourceViewModel vm = new ResourceViewModel();

			vm.LearningResourceID = res.LearningResourceID;
			vm.BaseInfo = model;

			return vm;
		}

		public ResourceViewModel SaveChildElements(ResourceChildInfo model, ResourceViewModel viewModel)
		{
			Resource res = Mapper.Map<ResourceBaseInfo, Resource>(viewModel.BaseInfo);

			res.LearningResourceLaunchURL = model.LaunchURL;
			res.ParentLearningResourceID = model.ParentID;
			res = SetAuditData(res);

			// Save everything...
			context.Resources.Add(res);
			context.SaveChanges();

			model.LearningResourceID = res.LearningResourceID;

			viewModel.ChildrenResources.Add(model);

			return viewModel;			
		}

		public ResourceViewModel SaveMetaData(ResourceMetaData model, ResourceViewModel viewModel)
		{
			Resource parent = context.Resources.SingleOrDefault(w => w.LearningResourceID == viewModel.LearningResourceID);
			bool findExistingVersions = false;

			if (parent != null)
			{
				if (findExistingVersions)
				{
					// Find out if an older version of this course exists...
					if (context.Resources.Any(w => w.LearningResourceTitle == viewModel.BaseInfo.ManifestResourceTitle))
					{
						Resource res = context.Resources.FirstOrDefault(w => w.LearningResourceTitle.ToLower() == viewModel.BaseInfo.ManifestResourceTitle.ToLower());

						if (res != null)
						{
							// We have an previous version take their ancillary data or assign to their units...

							//TODO: POSSIBLY USE ADAPTER PATTER TO LOG IF RESOURCE EXISTS....
						}
					}

				}

				// Only add resource if we have a Unit...
				Unit unit = null;
				if (viewModel.BaseInfo.DefaultCategoryStructure != String.Empty)
				{
					unit = SetUnit(viewModel.BaseInfo.DefaultCategoryStructure);
				}
				else
				{
					unit = SetUnit("Misc");
				}
				parent.LearningResourcesUnits.Add(new ResourcesUnit { UnitID = unit.UnitID, LearningResourcesUnitAddedOn = DateTime.Today, LearningResourcesUnitAddedByUserID = 1, LearningResourcesUnitDisplayOrder = 10 });
				context.SaveChanges();

				return viewModel;

			}
			else
			{
				throw new Exception("Unable to find parent record");
			}			
		}

		public bool DoesResourceExist(string title)
		{
			return context.Resources.Any(w => w.LearningResourceTitle == title);
		}

		private Unit SetUnit(string structure)
		{
			Unit returnUnit = null;
			Unit currentUnit = null;

			// Do we have more than one category...
			string[] cats = structure.Split('\\');

			foreach (string item in cats)
			{
				// Do we have an category to add...
				if (item != string.Empty)
				{
					string newCatName = item.ToLower() + " (new)";
					currentUnit = context.Units.SingleOrDefault(w => w.UnitName.ToLower() == newCatName || w.UnitName.ToLower() == item.ToLower());

					// Does the category exist...
					if (currentUnit == null)
					{
						// No, so we need to add it but is this a sub category...
						if (returnUnit == null)
						{
							returnUnit = CreateUnit(item, null);
						}
						else
						{
							returnUnit = CreateUnit(item, returnUnit.UnitID);
						}
					}
					else
					{
						returnUnit = currentUnit;
					}

				}
			}

			return returnUnit;
		}

		private Unit CreateUnit(string name, int? parentID)
		{
			Unit unit = new Unit();
			var r = new Random();
			string reference = string.Empty;

			unit.UnitName = name + " (new)";
			reference = name.Substring(0, 3).ToUpper();

			// Quick check to see if unit ref exists...
			if (context.Units.Any(w => w.UnitReference == reference))
			{
				reference += "-" + r.Next();
			}

			unit.UnitReference = reference;
			unit.UnitIsOrganisationSpecific = false;			
			unit.UnitAddedOn = DateTime.Today;
			unit.UnitAddedByUserID = 1;
			unit.UnitAvailable = true;
			if (parentID.HasValue)
			{
				unit.UnitParentID = parentID.Value;
			}
			unit.UserLastUpdatingRecordID = 1;

			// Add unit to ILMCourse [Courseware View for Administrators]...
			context.ILMCoursesUnits.Add(new ILMCoursesUnit { ILMCourseID = 1, UnitID = unit.UnitID, ILMCoursesUnitAvailable = true, ILMCoursesUnitAddedOn = DateTime.Today, ILMCoursesUnitAddedByUserID = 1, UserLastUpdatingRecordID = 1 });

			context.Units.Add(unit);
			context.SaveChanges();

			return unit;

		}
		
		private void InitialiseMapping()
		{
			Mapper.CreateMap<ResourceBaseInfo, Resource>()
				.ForMember(dest => dest.LearningResourceTitle, opt => opt.MapFrom(src => src.Title))
				.ForMember(dest => dest.LearningResourceReference, opt => opt.MapFrom(src => src.Reference))
				.ForMember(dest => dest.LearningResourceSynopsis, opt => opt.MapFrom(src => src.Description))
				.ForMember(dest => dest.LearningResourcePublisher, opt => opt.MapFrom(src => src.Publisher))
				.ForMember(dest => dest.LearningResourcePublishDate, opt => opt.MapFrom(src => src.PublishDate))
				.ForMember(dest => dest.LearningResourceAuthor, opt => opt.MapFrom(src => src.Author))
				.ForMember(dest => dest.LearningResourceLength, opt => opt.MapFrom(src => src.Length))
				.ForMember(dest => dest.LearningResourceCost, opt => opt.MapFrom(src => src.Cost))
				.ForMember(dest => dest.LearningResourceTrack, opt => opt.MapFrom(src => src.Tracked))
				.ForMember(dest => dest.LearningResourceScored, opt => opt.MapFrom(src => src.Scored))
				.ForMember(dest => dest.LearningResourceAvailable, opt => opt.MapFrom(src => src.IsActive))
				.ForMember(dest => dest.LearningResourceSCORMCoursePath, opt => opt.MapFrom(src => src.ScormCoursePath))
				.ForMember(dest => dest.LearningResourceSCORMIMSPath, opt => opt.MapFrom(src => src.ScormIMSPath))
				.ForMember(dest => dest.LearningResourceLaunchURL, opt => opt.MapFrom(src => src.AICCLaunchURL))
				.ForMember(dest => dest.UserLastUpdatingRecordID, opt => opt.MapFrom(src => src.UpdatedBy));
		}

		private Resource SetAuditData(Resource res)
		{
			res.LearningResourceAddedByUserID = 1;
			res.LearningResourceAddedOn = DateTime.Now;
			res.LearningResourceUpdatedOn = DateTime.Now;
			return res;
		}

	}
}
