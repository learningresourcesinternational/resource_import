﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ScormImport.DAL.Infrastructure;
using ScormImport.Models;

namespace ScormImport.DAL.Repos
{
	public class AcademyRepository : IProductRepository
	{
		AcademyEntities context = new AcademyEntities();

		public ResourceViewModel SaveParent(ResourceBaseInfo model)
		{
			// Map base info to learning resource...			
			InitialiseMapping();
			LearningResource res = Mapper.Map<ResourceBaseInfo, LearningResource>(model);
	
			// Academy specifics...
			res.MediaTypeID = 5;
			res.CourseTypeID = 129;
			res.LearningResourceCostCurrencyTypeID = 30;
			res.SystemObjectsStatusID = 31;
			res.LearningResourceTargetCompletionDays = 0;
			res.LearningResourceTargetCompletionHours = 0;			
			res.ParentLearningResourceID = 0;
			res = SetAuditData(res);

			// Save everything here...
			context.LearningResources.Add(res);
			context.SaveChanges();

			// Create View Model...
			ResourceViewModel vm = new ResourceViewModel();

			vm.LearningResourceID = res.LearningResourceID;
			vm.BaseInfo = model;

			return vm;
		}

		public ResourceViewModel SaveChildElements(ResourceChildInfo model, ResourceViewModel viewModel)
		{
			LearningResource res = Mapper.Map<ResourceBaseInfo, LearningResource>(viewModel.BaseInfo);

			res.LearningResourceLaunchURL = model.LaunchURL;
			res.ParentLearningResourceID = model.ParentID;
			res = SetAuditData(res);

			// Save everything...
			context.LearningResources.Add(res);
			context.SaveChanges();

			model.LearningResourceID = res.LearningResourceID;

			viewModel.ChildrenResources.Add(model);

			return viewModel;			
		}

		public ResourceViewModel SaveMetaData(ResourceMetaData model, ResourceViewModel viewModel)
		{
			LearningResource parent = context.LearningResources.SingleOrDefault(w => w.LearningResourceID == viewModel.LearningResourceID);
			List<LearningResourcesCategory> cats = null;			

			if (parent != null)
			{				
					// Find out if an older version of this course exists...
					if (context.LearningResources.Any(w => w.LearningResourceTitle == viewModel.BaseInfo.ManifestResourceTitle))
					{
						LearningResource res = context.LearningResources.FirstOrDefault(w => w.LearningResourceTitle.ToLower() == viewModel.BaseInfo.ManifestResourceTitle.ToLower());

						if (res != null)
						{
							// We have an previous version take their ancillary data...
							if (res.EvaluationFormID.HasValue)
							{
								model.EvaluationID = res.EvaluationFormID.Value;
							}
						}
					}

					// Update parent record...
					parent.EvaluationFormID = model.EvaluationID;

				// Only add resource if we have a category...
					LearningCategory cat = null;
					if (viewModel.BaseInfo.DefaultCategoryStructure != String.Empty)
					{
						cat = SetLearningCategory(viewModel.BaseInfo.DefaultCategoryStructure);
					}
					else
					{
						cat = SetLearningCategory("Misc");
					}
					parent.LearningResourcesCategories.Add(new LearningResourcesCategory { LearningCategoryID = cat.LearningCategoryID, LearningResourcesCategoryAddedOn = DateTime.Today, LearningResourcesCategoryAddedByUserID = 2 });
					context.SaveChanges();

					return viewModel;
				
			}
			else 
			{
				throw new Exception("Unable to find parent record");
			}			
		}

		public bool DoesResourceExist(string title)
		{
			return context.LearningResources.Any(w => w.LearningResourceTitle == title);
		}

		private void InitialiseMapping()
		{
			Mapper.CreateMap<ResourceBaseInfo, LearningResource>()
				.ForMember(dest => dest.LearningResourceTitle, opt => opt.MapFrom(src => src.Title))
				.ForMember(dest => dest.LearningResourceReference, opt => opt.MapFrom(src => src.Reference))
				.ForMember(dest => dest.LearningResourceSynopsis, opt => opt.MapFrom(src => src.Description))
				.ForMember(dest => dest.LearningResourcePublisher, opt => opt.MapFrom(src => src.Publisher))
				.ForMember(dest => dest.LearningResourcePublishDate, opt => opt.MapFrom(src => src.PublishDate))
				.ForMember(dest => dest.LearningResourceAuthor, opt => opt.MapFrom(src => src.Author))
				.ForMember(dest => dest.LearningResourceLength, opt => opt.MapFrom(src => src.Length))
				.ForMember(dest => dest.LearningResourceCost, opt => opt.MapFrom(src => src.Cost))
				.ForMember(dest => dest.LearningResourceCanBeBorrowed, opt => opt.MapFrom(src => src.CanBeBorrowed))
				.ForMember(dest => dest.LearningResourceTrack, opt => opt.MapFrom(src => src.Tracked))
				.ForMember(dest => dest.LearningResourceScored, opt => opt.MapFrom(src => src.Scored))
				.ForMember(dest => dest.LearningResourceCPDEligible, opt => opt.MapFrom(src => src.CPDEligible))
				.ForMember(dest => dest.LearningResourceCPDEligiblePoints, opt => opt.MapFrom(src => src.CPDEligiblePoints))
				.ForMember(dest => dest.LearningResourceISBN, opt => opt.MapFrom(src => src.ISBN))
				.ForMember(dest => dest.LearningResourceAvailable, opt => opt.MapFrom(src => src.IsActive))
				.ForMember(dest => dest.LearningResourceOverrideApproval, opt => opt.MapFrom(src => src.OverrideApproval))
				.ForMember(dest => dest.LearningResourceRequiresFeedback, opt => opt.MapFrom(src => src.RequiresFeedback))
				.ForMember(dest => dest.LearningResourceSCORMCoursePath, opt => opt.MapFrom(src => src.ScormCoursePath))
				.ForMember(dest => dest.LearningResourceSCORMIMSPath, opt => opt.MapFrom(src => src.ScormIMSPath))
				.ForMember(dest => dest.LearningResourceLaunchURL, opt => opt.MapFrom(src => src.AICCLaunchURL))
				.ForMember(dest => dest.UserLastUpdatingRecordID, opt => opt.MapFrom(src => src.UpdatedBy));
		}

		private LearningCategory SetLearningCategory(string structure)
		{
			LearningCategory returnCat = null;
			LearningCategory currentCat = null;
			
			// Do we have more than one category...
			string[] cats = structure.Split('\\');		

			foreach (string item in cats)
			{
				// Do we have an category to add...
				if (item != string.Empty)
				{
					string newCatName = item.ToLower() + " (new)";
					currentCat = context.LearningCategories.SingleOrDefault(w => w.LearningCategoryName.ToLower() == newCatName || w.LearningCategoryName.ToLower() == item.ToLower());					

					// Does the category exist...
					if (currentCat == null)
					{
						// No, so we need to add it but is this a sub category...
						if (returnCat == null)
						{
							returnCat = CreateLearningCategory(item, null);
						}
						else
						{
							returnCat = CreateLearningCategory(item, returnCat.LearningCategoryID);
						}
					}
					else
					{
						returnCat = currentCat;
					}

				}
			}

			return returnCat;				
		}

		private LearningCategory CreateLearningCategory(string name, int? parentID)
		{
			LearningCategory cat = new LearningCategory();

			cat.LearningCategoryName = name + " (new)";
			cat.LearningCategoryRef = name.Substring(0, 3).ToUpper();
			cat.LearningCategoryDescription = "";
			cat.LearningCategoryAddedOn = DateTime.Today;
			cat.LearningCategoryAddedByUserID = 1;
			cat.LearningCategoryAvailable = true;
			if (parentID.HasValue)
			{
				cat.ParentCategoryID = parentID;
			}
			cat.SystemObjectsStatusID = 21;
			cat.UserLastUpdatingRecordID = 1;

			context.LearningCategories.Add(cat);
			context.SaveChanges();

			return cat;

		}

		private LearningResource SetAuditData(LearningResource res)
		{
			res.LearningResourceAddedByUserID = 1;
			res.LearningResourceAddedOn = DateTime.Now;
			res.LearningResourceUpdatedOn = DateTime.Now;
			return res;
		}
	}
}
