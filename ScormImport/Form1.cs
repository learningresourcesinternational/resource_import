﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ninject;
using ScormImport.DAL;
using ScormImport.DAL.Repos;
using ScormImport.Domain;

namespace ScormImport
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			
			// Set up Ninject IOC...
			//var kernel = new StandardKernel();
			//kernel.Bind<IRepository>().To<AcademyRepository>();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void btnFind_Click(object sender, EventArgs e)
		{
			if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				lblSelectedPath.Text = folderBrowserDialog1.SelectedPath;
				FindManifests();
				FindAICCFiles();
				FindZips();
			}
		}

		private void FindManifests()
		{
			// Do we have a folder to search...
			if (folderBrowserDialog1.SelectedPath.Length > 0)
			{
				int fileCount = DirectorySearch(folderBrowserDialog1.SelectedPath, "imsmanifest.xml").Count();
				lblFoundManifests.Text = fileCount.ToString();
				if (fileCount == 0)
				{
					MessageBox.Show("No manifest files found.");
				}
			}
		}

		private void FindAICCFiles()
		{
			// Do we have a folder to search...
			if (folderBrowserDialog1.SelectedPath.Length > 0)
			{
				int fileCount = DirectorySearch(folderBrowserDialog1.SelectedPath, "course.des").Count();
				lblAiccFiles.Text = fileCount.ToString();
				if (fileCount == 0)
				{
					MessageBox.Show("No AICC files found.");
				}
			}
		}
		
		private void FindZips()
		{
			// Do we have a folder to search...
			if (folderBrowserDialog1.SelectedPath.Length > 0)
			{
				int fileCount = DirectorySearch(folderBrowserDialog1.SelectedPath, "*.zip").Count();
				lblZip.Text = fileCount.ToString();
				if (fileCount == 0)
				{
					MessageBox.Show("No ZIP files found.");
				}
			}
		}

		private string[] DirectorySearch(string path, string ext)
		{
			string[] files = Directory.GetFiles(path, ext, SearchOption.AllDirectories);
			return files; 
		}

		private void ExtractZips(string path)
		{
			// Do we have a folder to search...
			if (folderBrowserDialog1.SelectedPath.Length > 0)
			{
				string[] zips = DirectorySearch(folderBrowserDialog1.SelectedPath, "*.zip");

				foreach (var file in zips)
				{					
					DirectoryInfo dir = Directory.GetParent(file);
					string extractPath = dir.FullName + "\\" + dir.Name;

					// Has the zip already been extracted...
					if (!Directory.Exists(extractPath))
					{
						ZipFile.ExtractToDirectory(file, extractPath);
					}
				}
			}
		}

		private ScormItem GetFirstManifestDetails()
		{
			string manifest = DirectorySearch(folderBrowserDialog1.SelectedPath, "imsmanifest.xml").FirstOrDefault();

			SCOFacade scorm = new SCOFacade(manifest);
			ScormItem scormItem = scorm.Items.FirstOrDefault();
			return scormItem;
		}

		private void btnSample_Click(object sender, EventArgs e)
		{
			// Do we have a folder to search...
			if (folderBrowserDialog1.SelectedPath.Length > 0)
			{
				ScormItem item = GetFirstManifestDetails();
				
				MessageBox.Show("Resource Name : " + item.Title);
			}
		}

		private void btnImport_Click(object sender, EventArgs e)
		{
			btnImport.Visible = false;						

			// Check all fields have been filled in...
			if (isValid())
			{
				// Do we have a folder to search...
				if (folderBrowserDialog1.SelectedPath.Length > 0)
				{
					ImportFacade importer;

					if (lstCourseType.SelectedItem.ToString() == "SCORM")
					{
						// Import SCORM...
						string[] manifest = DirectorySearch(folderBrowserDialog1.SelectedPath, "imsmanifest.xml");
						importer = new ImportFacade(manifest, ProductRepo(), txtLaunchPath.Text, txtCategory.Text, txtDescription.Text);

						importer.ImportScormResources();
					}

					if (lstCourseType.SelectedItem.ToString() == "AICC")
					{
						// Import AICC...
						string[] aiccFiles = DirectorySearch(folderBrowserDialog1.SelectedPath, "course.des");
						importer = new ImportFacade(aiccFiles, ProductRepo(), txtLaunchPath.Text, txtCategory.Text, txtDescription.Text);

						importer.ImportAICCResources();
					}
					MessageBox.Show("Finished");
				}
			}
			else
			{
				MessageBox.Show("Please sure all required fields are filled in.");
			}
			btnImport.Visible = true;
		}

		private IProductRepository ProductRepo()
		{
			IProductRepository repo = null;

			switch (lstProduct.SelectedItem.ToString().ToUpper())
			{
				case "ACADEMY":
					repo = new AcademyRepository();
					break;
				case "BPM":
					repo = new BPMRepository();
					break;
				default:
					throw new Exception("Unable to find required repository.");
			}

			return repo;
		}

		private void btnClose_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void btnUnzip_Click(object sender, EventArgs e)
		{
			ExtractZips(folderBrowserDialog1.SelectedPath);
			FindZips();
			MessageBox.Show("Extract Finished");
		}

		private bool isValid()
		{
			if (txtCategory.Text.Length <= 0 || txtLaunchPath.Text.Length <= 0 || lstCourseType.SelectedItem == null || lstProduct.SelectedItem == null)
			{
				return false;
			}
			return true;
		}

	}
}
