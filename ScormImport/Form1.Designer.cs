﻿namespace ScormImport
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.txtCSV = new System.Windows.Forms.TextBox();
			this.btnFind = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.label2 = new System.Windows.Forms.Label();
			this.lblSelectedPath = new System.Windows.Forms.Label();
			this.btnImport = new System.Windows.Forms.Button();
			this.btnClose = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.lblFoundManifests = new System.Windows.Forms.Label();
			this.btnSample = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.lblZip = new System.Windows.Forms.Label();
			this.btnUnzip = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.lblAiccFiles = new System.Windows.Forms.Label();
			this.lstCourseType = new System.Windows.Forms.ComboBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.txtCategory = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.txtLaunchPath = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.txtDescription = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.lstProduct = new System.Windows.Forms.ComboBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
			this.pictureBox2.Location = new System.Drawing.Point(708, 7);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(207, 110);
			this.pictureBox2.TabIndex = 3;
			this.pictureBox2.TabStop = false;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
			this.pictureBox1.Location = new System.Drawing.Point(12, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(220, 105);
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// txtCSV
			// 
			this.txtCSV.Enabled = false;
			this.txtCSV.Location = new System.Drawing.Point(151, 248);
			this.txtCSV.Name = "txtCSV";
			this.txtCSV.Size = new System.Drawing.Size(222, 20);
			this.txtCSV.TabIndex = 10;
			// 
			// btnFind
			// 
			this.btnFind.Location = new System.Drawing.Point(379, 248);
			this.btnFind.Name = "btnFind";
			this.btnFind.Size = new System.Drawing.Size(75, 23);
			this.btnFind.TabIndex = 9;
			this.btnFind.Text = "Browse";
			this.btnFind.UseVisualStyleBackColor = true;
			this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(21, 247);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(61, 19);
			this.label1.TabIndex = 8;
			this.label1.Text = "Folder:*";
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(511, 180);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(101, 19);
			this.label2.TabIndex = 12;
			this.label2.Text = "Selected Path:";
			// 
			// lblSelectedPath
			// 
			this.lblSelectedPath.AutoSize = true;
			this.lblSelectedPath.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSelectedPath.Location = new System.Drawing.Point(637, 180);
			this.lblSelectedPath.Name = "lblSelectedPath";
			this.lblSelectedPath.Size = new System.Drawing.Size(110, 19);
			this.lblSelectedPath.TabIndex = 13;
			this.lblSelectedPath.Text = "- non selected -";
			// 
			// btnImport
			// 
			this.btnImport.Location = new System.Drawing.Point(759, 543);
			this.btnImport.Name = "btnImport";
			this.btnImport.Size = new System.Drawing.Size(75, 23);
			this.btnImport.TabIndex = 14;
			this.btnImport.Text = "Import";
			this.btnImport.UseVisualStyleBackColor = true;
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			// 
			// btnClose
			// 
			this.btnClose.Location = new System.Drawing.Point(840, 543);
			this.btnClose.Name = "btnClose";
			this.btnClose.Size = new System.Drawing.Size(75, 23);
			this.btnClose.TabIndex = 15;
			this.btnClose.Text = "Close";
			this.btnClose.UseVisualStyleBackColor = true;
			this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(511, 218);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(121, 19);
			this.label4.TabIndex = 16;
			this.label4.Text = "Found Manifests:";
			// 
			// lblFoundManifests
			// 
			this.lblFoundManifests.AutoSize = true;
			this.lblFoundManifests.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFoundManifests.Location = new System.Drawing.Point(638, 218);
			this.lblFoundManifests.Name = "lblFoundManifests";
			this.lblFoundManifests.Size = new System.Drawing.Size(17, 19);
			this.lblFoundManifests.TabIndex = 17;
			this.lblFoundManifests.Text = "0";
			// 
			// btnSample
			// 
			this.btnSample.Location = new System.Drawing.Point(678, 543);
			this.btnSample.Name = "btnSample";
			this.btnSample.Size = new System.Drawing.Size(75, 23);
			this.btnSample.TabIndex = 18;
			this.btnSample.Text = "Sample";
			this.btnSample.UseVisualStyleBackColor = true;
			this.btnSample.Click += new System.EventHandler(this.btnSample_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(511, 274);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(83, 19);
			this.label5.TabIndex = 19;
			this.label5.Text = "Found Zips:";
			// 
			// lblZip
			// 
			this.lblZip.AutoSize = true;
			this.lblZip.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblZip.Location = new System.Drawing.Point(638, 274);
			this.lblZip.Name = "lblZip";
			this.lblZip.Size = new System.Drawing.Size(17, 19);
			this.lblZip.TabIndex = 20;
			this.lblZip.Text = "0";
			// 
			// btnUnzip
			// 
			this.btnUnzip.Location = new System.Drawing.Point(673, 274);
			this.btnUnzip.Name = "btnUnzip";
			this.btnUnzip.Size = new System.Drawing.Size(75, 23);
			this.btnUnzip.TabIndex = 21;
			this.btnUnzip.Text = "Unzip";
			this.btnUnzip.UseVisualStyleBackColor = true;
			this.btnUnzip.Click += new System.EventHandler(this.btnUnzip_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(511, 246);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(121, 19);
			this.label6.TabIndex = 22;
			this.label6.Text = "Found AICC Files:";
			// 
			// lblAiccFiles
			// 
			this.lblAiccFiles.AutoSize = true;
			this.lblAiccFiles.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAiccFiles.Location = new System.Drawing.Point(638, 246);
			this.lblAiccFiles.Name = "lblAiccFiles";
			this.lblAiccFiles.Size = new System.Drawing.Size(17, 19);
			this.lblAiccFiles.TabIndex = 23;
			this.lblAiccFiles.Text = "0";
			// 
			// lstCourseType
			// 
			this.lstCourseType.FormattingEnabled = true;
			this.lstCourseType.Items.AddRange(new object[] {
            "SCORM",
            "AICC"});
			this.lstCourseType.Location = new System.Drawing.Point(152, 210);
			this.lstCourseType.Name = "lstCourseType";
			this.lstCourseType.Size = new System.Drawing.Size(121, 21);
			this.lstCourseType.TabIndex = 24;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(21, 212);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 19);
			this.label7.TabIndex = 25;
			this.label7.Text = "Course Type:*";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.ForeColor = System.Drawing.Color.DarkBlue;
			this.label8.Location = new System.Drawing.Point(511, 144);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(72, 23);
			this.label8.TabIndex = 26;
			this.label8.Text = "Preview";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.DarkBlue;
			this.label3.Location = new System.Drawing.Point(21, 144);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(71, 23);
			this.label3.TabIndex = 11;
			this.label3.Text = "Settings";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(21, 287);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(79, 19);
			this.label9.TabIndex = 27;
			this.label9.Text = "Category:*";
			// 
			// txtCategory
			// 
			this.txtCategory.Location = new System.Drawing.Point(152, 286);
			this.txtCategory.Name = "txtCategory";
			this.txtCategory.Size = new System.Drawing.Size(222, 20);
			this.txtCategory.TabIndex = 28;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(21, 327);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(127, 19);
			this.label10.TabIndex = 29;
			this.label10.Text = "Virtual Directory:*";
			// 
			// txtLaunchPath
			// 
			this.txtLaunchPath.Location = new System.Drawing.Point(152, 328);
			this.txtLaunchPath.Name = "txtLaunchPath";
			this.txtLaunchPath.Size = new System.Drawing.Size(222, 20);
			this.txtLaunchPath.TabIndex = 30;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label11.Location = new System.Drawing.Point(22, 368);
			this.label11.MaximumSize = new System.Drawing.Size(100, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(87, 38);
			this.label11.TabIndex = 31;
			this.label11.Text = "Generic Description:";
			// 
			// txtDescription
			// 
			this.txtDescription.Location = new System.Drawing.Point(152, 369);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(222, 178);
			this.txtDescription.TabIndex = 32;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label12.Location = new System.Drawing.Point(161, 348);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(143, 15);
			this.label12.TabIndex = 33;
			this.label12.Text = "(Jenison, Jenison-Cloud...)";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
			this.label13.Location = new System.Drawing.Point(161, 306);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(185, 15);
			this.label13.TabIndex = 34;
			this.label13.Text = "(E.g Express or NewCats\\\\Express)";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label14.Location = new System.Drawing.Point(22, 180);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(70, 19);
			this.label14.TabIndex = 36;
			this.label14.Text = "Product:*";
			// 
			// lstProduct
			// 
			this.lstProduct.FormattingEnabled = true;
			this.lstProduct.Items.AddRange(new object[] {
            "Academy",
            "BPM"});
			this.lstProduct.Location = new System.Drawing.Point(153, 178);
			this.lstProduct.Name = "lstProduct";
			this.lstProduct.Size = new System.Drawing.Size(121, 21);
			this.lstProduct.TabIndex = 35;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.ClientSize = new System.Drawing.Size(927, 578);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.lstProduct);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.txtDescription);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.txtLaunchPath);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.txtCategory);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.lstCourseType);
			this.Controls.Add(this.lblAiccFiles);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.btnUnzip);
			this.Controls.Add(this.lblZip);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.btnSample);
			this.Controls.Add(this.lblFoundManifests);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.btnClose);
			this.Controls.Add(this.btnImport);
			this.Controls.Add(this.lblSelectedPath);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtCSV);
			this.Controls.Add(this.btnFind);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox2);
			this.Controls.Add(this.pictureBox1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox2;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox txtCSV;
		private System.Windows.Forms.Button btnFind;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label lblSelectedPath;
		private System.Windows.Forms.Button btnImport;
		private System.Windows.Forms.Button btnClose;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label lblFoundManifests;
		private System.Windows.Forms.Button btnSample;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label lblZip;
		private System.Windows.Forms.Button btnUnzip;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label lblAiccFiles;
		private System.Windows.Forms.ComboBox lstCourseType;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtCategory;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txtLaunchPath;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtDescription;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.ComboBox lstProduct;
	}
}

